const express = require('express')//express 웹 애플리케이션 프레임워크
const fs = require('fs')//require는 키워드를 사용하여 여타 다른 변수를 할당하듯 모듈을 불러운다. fs파일을 읽는 모듈
const ejs = require('ejs')//http와 같은 스크립트 
const mysql = require('mysql')//DB 
const bodyParser = require('body-parser')

const cors = require('cors'); // CROS

const client = mysql.createConnection({
   host : 'localhost', //host ip넘버
  port : 3307,
  user : 'root', // 유저 id
  password : 'Maria0425!', // 처음 설정때 비밀번호를 쓰면 오류가 뜸 비밀번호 변경을 한번해야함
  database : 'ionic_db' // DB이름 
  });

const app = express()

// CORS 설정
app.use(cors());
app.use(bodyParser.json());//body를 파싱받는 방식을 .json

app.get('/products/:id', function (req, res, next) {
  res.json({msg: 'This is CORS-enabled for all origins!'})
});




// 요청 = request = req
// 응담 = response = res
// 에러 = error = err

app.listen(52273, function () {//function(평션) 함수선언
  console.log('Server is running at : http://127.0.0.1:52273')
});//로컬DB 52273포트로 돌림

app.get('/getContents', function (req, res) {// 전체리스트
  const body = req.body
  console.log('전체 조회');
  
    client.query('select * from NEWS ',[body.id], function (err, results) {
   	res.send(results);//postman
      
      })
  }); 

app.get('/getContent', function (req, res) {// 리스트 
  const body = req.body
  console.log('단일 조회');
  console.log(body);
  
    client.query('select * from NEWS where id = ? ',[body.id], function (err, results) {
   	res.send(results);//postman
      
      })
  });
// '/' 는 뒤에 붙는 코드에 따라 실행유무
app.post('/delete', function (req, res) {//list.ejs 24번째 줄 get id
  console.log('삭제');
  const body = req.body
  console.log(body);
  client.query('delete from NEWS where id=?', [body.id],
   function (err,results) {
    res.send(results)//postman
  })
})

app.post('/insert', function (req, res) {
  console.log('입력');
  const body = req.body;
  console.log(req.body);
  client.query('insert into NEWS (title, contents, img, created) values (?, ?, ?, NOW());', [
    body.title,
    body.contents,
    body.img
  ], function(err,results) {// err,results 넣어줌 postman
    res.send(results)
  })
});

app.post('/edit', function (req, res) {
  console.log('수정');
  const body = req.body
  console.log(body);
  client.query('update NEWS SET title=?, contents=?, img=? where id=?',[
    body.title, 
    body.contents, 
    body.img,
    body.id
  ], function (err,results) {
    //res.redirect('/')
    res.send(results)
  })
})