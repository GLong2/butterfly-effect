var mysql = require('mysql');//mysql을 가져온다.

var db = mysql.createConnection({ //함수선언
  host : 'localhost', //host ip넘버
  //port : 3306,
  //path : 'usr/local/mariadb10/bin',
  user : 'root', // 유저 id
  password : '1111', // 처음 설정때 비밀번호를 쓰면 오류가 뜸 비밀번호 변경을 한번해야함
  database : 'Bood' // DB이름 
});

db.connect(); // MySQL 연결

var sql = 'SELECT * FROM NEWS'; // NEWS라는 테이블을 전부 선택해서 본다.

// db.query(sql, function(err, rows,fields){//괄호안에 SQL문,첫번째인자에대한 모든 작업이 끝난 후 콜백함수 실행
//     if(err){
//         console.log(err);
//     }else{
//        console.log('rows',rows);// rows안에는 배열안에 객체의 담긴 형태로 정보가 담긴 것을 볼 수 있다
//      //   console.log('fields',fields);// 필드명(컬럼) 어떤 컬럼이 존재하고 그 컬럼에 대한 상세정보가 나와있다.
//     }
// });

//SELECT 문
var SELECT = 'SELECT * FROM NEWS';
db.query(sql,function(err,rows,fields){ // rows는 행이라는 뜻
    if(err){
        console.log(err);
    }else{
        for(var i=0; i< rows.length; i++){
            console.log(rows[i].title+":"+rows[i].contents);
        }
    }
});

//INSERT

//sql문을 하드코딩 하지 않고, ? 라는 치환자를 두어 코딩함
// var sql = 'INSERT INTO NEWS (title, contents, created) VALUES(?, ?, NOW())';
// var params = ['테스트', '한글확인'];//파라미터를 값들로 줌(배열로 생성)
// db.query(sql, params, function(err, rows, fields){// 쿼리문 두번째 인자로 파라미터로 전달함(값들을 치환시켜서 실행함. 보안과도 밀접한 관계가 있음(sql injection attack))
//     if(err) console.log(err);
//     console.log(rows.insertId);
// });

//UPDATE
// var sql = 'UPDATE NEWS SET title="?", contents="?" WHERE id=?';
// var params = ['수정', '확인', 7];
// db.query(sql, params, function(err, rows, fields){
//     if(err) console.log(err);
//     console.log(rows);
// });


//DELETE
// var sql = 'DELETE FROM NEWS WHERE id="?"';
// var params = [12]; // 숫자는 없앨 id 값을 넣으면 된다.
// db.query(sql, params, function(err, rows, fields){
//     if(err) console.log(err);
//     console.log(rows);
// });

db.end(); //작업이 끝난 후 연결 끊기