var mysql = require('mysql');//mysql을 가져온다.

var db = mysql.createConnection({ //함수선언
  host : 'localhost', //host ip넘버
  user : 'root', // 유저 id
  password : '1111', // 처음 설정때 비밀번호를 쓰면 오류가 뜸 비밀번호 변경을 한번해야함
  database : 'Bood' // DB이름 
});

db.connect(); // MySQL 연결

var sql = 'SELECT * FROM NEWS'; // NEWS라는 테이블을 전부 선택해서 본다.

db.query(sql, function(err, rows,fields){//괄호안에 SQL문,첫번째인자에대한 모든 작업이 끝난 후 콜백함수 실행
    if(err){
        console.log(err);
    }else{
       console.log('rows',rows);// rows안에는 배열안에 객체의 담긴 형태로 정보가 담긴 것을 볼 수 있다
     //   console.log('fields',fields);// 필드명(컬럼) 어떤 컬럼이 존재하고 그 컬럼에 대한 상세정보가 나와있다.
    }
});

db.end(); //작업이 끝난 후 연결 끊기