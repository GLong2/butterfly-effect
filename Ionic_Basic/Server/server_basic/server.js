//1. 서버 사용을 위해서 http 모듈을 http 변수에 담는다. (모듈과 변수의 이름은 달라도 된다.)
var http = require('http');
//웹서버를 실행하기 위해서는 http 모듈을 require로 불러와야한다. require는 다른언어의 import와 유사한 기능을 한다.
//node.jsSMS require 후에 해당 모듈을 http 라는 변수에 담은 후 하나의 독립적 객체로 사용함
//http 모듈에 정의된 모든 기능이 변수 http로 생성되는데 변수명은 꼭 http로 하지 않아도됨

// var http = require('http');
// http 모둘에 정의되어 있는 createServer 함수로 서버를 생성합니다/
// 일반적인 함수의 사용법은 아래와 같다.

// function name0fFunction(parameters){
    // 실행로직
//}

//2. http 모듈로 서버를 생성한다.
// 아래와 같이 작성하면 서버를 생성한 후, 사용자로 부터 http 요청이 들어오면 function 블럭내부의 코드를 싱행해서 응답한다.
var server = http.createServer(function(request,response){

    response.writeHead(200,{'Content-Type':'text/html'});
    response.end('Hello node.js');
});
//createServer()에 파라미터로 입력되는 function(request,response){}는 함수명이 없다.
// 함수명이 없이 function 이라고만 작성된 파라미터는 이벤트 발생시에 callback됩니다. 즉 생성된 서버로 어떤 요청이 들어오면 function 내부의 로직이 실행되면서 function 내부에 선언한 request와 response라는 이름으로 사용할 수 있는 값을 넘겨줍니다.
// 따러서 function 블럭 {} 내부에서는 request 와 response로 넘어오는 어떤 값을 사용 할 수 있게 됩니다.

//function 내부 로직을 살펴보면 response로 넘어온 값으로 함수를 실행 합니다. 즉 callback 되었을 때 response 에 담겨저 오는 값은 require로 가져온 http 모듈처럼 내부적으로 함수를 가지고 있는 객체라는 의미이다.
//response 객체는 서버로 웹브라우저나 또는 앱으로 부터 어떤 요청이 있을 때 요청한 사용자 측으로 값을 반환해 줄 때 사용하는 객체입니다.
//반대로 request 객체는 사용자가 요청한 내용이 담겨있는 객체 입니다.

//response.writeHead(200, {'Content-Type':'text/html'});
//코드에서 response 객체를 사용해 사용자 측으로 반환값을 넘겨주는데, 먼저 writeHead()라는 함수에 첫번째는 200이라는 숫자값을 두번째 {}중괄호 안은 {'키':'값'}형태의 값을 담는다.

// 200이라는 숫자값은 웹서버에 들어오는 어떤 요청에 대해 정상적으로 값을 리턴할 때 사용하는 http 상태코드이다.(https://javafa.gitbooks.io/nodejs_server_basic/content/appendix1_http_status.html) <- 상태코드 정리
// 오류 없이 서버에 처리가 정상적으로 완료되면 200 코드를 담아서 응답헤더를 설정해 주게 된다.
//응답헤더를 브라우저로 예를 들어 설명하면 서버로 부터 반환되는 값의 대한 기본 정보를 담고 있는 것으로 브라우저 화면에는 나타나지 않는 값이다.
// 브라우저는 header 값을 보고 서버에서 넘어온 값이 어떤 형태인지를 파악하고 실제 값을 header 에 세팅된 설정에 맞게 보여주게 된다.


//두번째 {'Content-Type':'text/html'}값은 서버측에서 보내주는 컨텐츠의 타입이 텍스트이고, html 형태라는 것을 정의합니다. 브라우저에서는 이 헤더를 기준으로 아래 코드에서 보여주는 값을 html 형태로 화면에 출력해 준다.
//{} 블럭형태로 값이 전달되는 경우에는 해당 블럭에 복수개의 값이 담길 수 있다는 의미이다.
// 두번째 값은 Content-Type 이라는 키값 외에 Authorization,Cookie등 다양한 값을 지정할 수 있다.

//response.end('Hello node.js!!');
//end()라는 함수에 'Hello node.js!! 라는 실제 컨텐츠를 담아서 브라우저 측에 전달한다.
// 실제 코드 값을 end()함수로 전달하면 브라우저는 해당 컨텐츠를 받은 후 html 형태로 화면에 출력해 줍니다.

//3. listen 함수로 8080 포트를 가진 서버를 실행한다. 서버가 실행된 것을 콘솔창에서 확인하기 위해 'Server is running...'로 표시
server.listen(8080,function(){
    console.log('Server is running...');
});