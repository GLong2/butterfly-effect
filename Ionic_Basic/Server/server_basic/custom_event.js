// 이벤트를 가진 객체만들기

// 1. 이벤트가 정의되 있는 events 모듈 생성. 이전 버전의 process.EventEmitter() 는 deprecated!
var EventEmitter = require('events');
// require 모듈을 반환(리턴)해주는 명령어

// 2. 생성된 이벤트 모듈을 사용하기 위해 custom_object로 초기화
var custom_object = new EventEmitter();

// 3. events 모듈에 선언되어 있는 on() 함수를 재정의 하여 'call' 이벤트를 처리
custom_object.on('call',()=>{
    console.log('called events!');
});

// 4. call 이벤트를 강제로 발생
custom_object.emit('call');

// 4번줄은 함수 EventEmitter에게 events값을 require가 반환하여 넣어준다.
// 8번줄은 함수 custom_object에 EventEmitter값을 대입한다.
// 11 custom_object는 이벤트함수 on으로 call이 올대까지 대기하고 있다가 call이 오면 텍스트 called events!를 반환한다.
