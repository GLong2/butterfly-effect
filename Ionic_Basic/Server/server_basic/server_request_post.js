// HTTP 프로토콜의 구조
// HTTP 프로토콜은 브라우저에서 서버로 요청(REQUEST) 하거나 서버에서 브라우저로 응답 (RESPONSE)할 때 서로 데이터를 주고받게 되는데 이 데이터의 구조는 요청에 대한 설정정보가 담기는 HEADER와 실제 데이터가 담기는 BODY로 구성된다.

// // Request : 실제 자원요청에 대한 정보가 저장되는 부분
// POST /request/specific_datas.call HTTP/1.1

// // Request Header : 자원요청에 대한 설정정보, 요청하는 데이터타입, 요청자의 브라우저정보 등이 담긴다.            
// Accept: image/gif, image/xxbitmap, image/jpeg, image/pjpeg,
// application/xshockwaveflash, application/vnd.msexcel,
// application/vnd.mspowerpoint, application/msword, */*
// Referer: http://wahh-app.com/books/default.asp
// Accept-Language: en-gb,en-us;q=0.5
// Accept-Encoding: gzip, deflate
// User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)

// //Request Data : 실제 전달하고자 하는 데이터가 담긴다
// jfhdahiekljdklhfkha=e%3kljfhailjljfljalkjlkfjldjaf

// GET의 경우는 가장 윗줄의 주소부분 끝에 ?를 붙이고 필요한 변수와 값을 전달하지만 POST는 가장 아래의 Request Data에 해당하는 BODY 부분에 데이터를 담아 전달

var http = require('http');
var querystring = require('querystring');

var server = http.createServer(function(request,response){
  // 1. post로 전달된 데이터를 담을 변수를 선언
  var postdata = '';
  // 2. request객체에 on( ) 함수로 'data' 이벤트를 연결
  request.on('data', function (data) {
    // 3. data 이벤트가 발생할 때마다 callback을 통해 postdata 변수에 값을 저장
    postdata = postdata + data;
  });

  // 4. request객체에 on( ) 함수로 'end' 이벤트를 연결
  request.on('end', function () {
    // 5. end 이벤트가 발생하면(end는 한버만 발생한다) 3번에서 저장해둔 postdata 를 querystring 으로 객체화
    var parsedQuery = querystring.parse(postdata);
    // 6. 객체화된 데이터를 로그로 출력
    console.log(parsedQuery);
    // 7. 성공 HEADER 와 데이터를 담아서 클라이언트에 응답처리
    response.writeHead(200, {'Content-Type':'text/html'});
    response.end('var1의 값 = '+result);
  });

});

server.listen(8080, function(){
    console.log('Server is running...');
});



// 개념 
// POST처리에서 가장 특징적인 부분은 event처리다.
// node.js는 일반적인 웹서버의 thread 동기방식이 아닌 비동기 event 방식으로 데이터를 처리합니다. on() 함수를 사용해서 event를 등록하고 해당 event가 발생하는 시점에 로직을 실행하는 구조가 가장 대표적인 event 처리를 위한 코드이다

//request.on('data', function (data) {
    // request 객체에서 data 이벤트가 발생하면 data 변수를 callback 함수에 담아서 내부 로직을 실행합니다.
// });

// request 객체에 on 함수를 사용해서 'data 라는 이벤트를 catch합니다. 비슷한 구조로는 server.listen() 이라는 형태로 서버를 구동시키는 함수가 있는데 둘 사이의 차이점은 listen과 다르게 on은 이벤트의 명칭을 정의한다는 것이다.
//같은 형태로 end 이벤트도 처리할 수 있는데 data 이벤트는 전송하는 데이터의 크기가 길 경우 여러번에 나누어서 발생하지만 end 이벤트는 data 전송이 완료되었을 때 한번 만 발생한다.

//request.on('end', function () {
    // request 객체에서 end 이벤트가 발생하면 내부 로직을 실행합니다. end 이벤트는 callback 시에 전달되는 값이 없습니다.
// });
// 이렇게 코딩하면 위의 data이벤트가 완료되고 아래의 end 이벤트가 발생하므로 순차적으로 실행되게 됨 이벤트를 등록하게 되면 마치 하나의 서버처럼 이벤트가 발생할 때까지 해당 로직이 대기 하게 된다. 그리고 로직의 순서와 관계없이 이벤트가 발생했을때만 동작을 한다.