//Get 요청처리
// 주소값을 이용해 요청을 하는 방식
//서버로 값을 전달하기 위해서 서버측 주소 끝에?(물음표)를 붗이고 키1=값1&키2=값2의 형태로 요청
//http://naver.com/post.nhn?postId=345345&cafeId=12312314
//위와 같이 브라우저를 통해 naver.com/Post.nhn 주소로 요청을 하게 되는데 서버측에서는 ?(물음표) 다음의 값들을 변수와 값의 형태를 받아서 사용, 실제 주소값 뒤에 붙어가는 값을 Query String 이라고 한다.

//Query String은 주소형태로 요청할 수도 있고, HTML에 있는 form 태그를 사용해서 요청할 수도 있다.

//서버측에서 Query String을 처리하기 위해서 아래와 같이 코드를 작성하고, 프로젝트폴더에 server_request_get.js로 저장합니다 프로젝트 폴더에 여러개의 파일이 존재해도 선택적으로 하나만 실행할 수 있다.


var http = require('http');
// 1. 요청한 url을 객체로 만들기 위해서 url 모듈사용
var url = require('url');
// 2. 요청한 url 중에 Query String 을 객체로 만들기 위해 querystring 모듈 사용
var querystring = require('querystring');

var server = http.createServer(function(request,response){
    // 3. 콘솔화면에 로그 시작 부분을 출력
    console.log('--- log start ---');
    // 4. 브라우저에서 요청한 주소를 parsing 하여 객체화 후 출력
    var parsedUrl = url.parse(request.url);
    console.log(parsedUrl);
    // 5. 객체화된 url 중에 Query String 부분만 따로 객체화 후 출력
    var parsedQuery = querystring.parse(parsedUrl.query,'&','=',);
    console.log(parsedQuery);
    // 6. 콘솔화면에 로그 종료 부분을 출력
    console.log('--- log end ---');

    // response.writeHead(200,{'Content-Type':'text/html'});
    // response.end('Hello node.js!!');

    response.writeHead(200, {'Content-Type':'text/html'});
    response.end('var1의값=' + parsedQuery.var1 + ', var2의값=' + parsedQuery.var2 + ', var3의값=' +parsedQuery.var3);


});

server.listen(8080,function(){
    console.log('Server is running...');

});

//  로그분석
//--- log start ---
// 1. 이부분이 var parsedUrl = url.parse(주소) 함수로 주소값을 객체화 한 부분이다.
//    객체화 되기 때문에 parsedUrl.search 형태로 객체 내부의 변수값을 사용할 수 있다.
//    아래에서는 객체 내부의 query 라는 변수값을 가져와서 다시 객체화 한다.
// Url {
//     protocol: null,
//     slashes: null,
//     auth: null,
//     host: null,
//     port: null,
//     hostname: null,
//     hash: null,
//     search: '?var1=newData&var2=153&var3=testdata2017',
//     query: 'var1=newData&var2=153&var3=testdata2017',
//     pathname: '/',
//     path: '/?var1=newData&var2=153&var3=testdata2017',
//     href: '/?var1=newData&var2=153&var3=testdata2017' 

// 2. 이 부분이 var parsedQuery = querystring.parse(parsedUrl.query,'&','=')
//    역시 위의 parsedUrl 객체에서 query 라는 변수값을 다시 querystring 으로 parsing 하여 객체화하였다.
//    해당 객체는 parsedQuery.var1 형태로 객체내부의 값을 사용할 수 있게 됩니다.
// url과 querystring 모듈에 있는 parse( ) 함수로 클라이언트가 요청한 주소값을 javascript 객체로 만들어서 사용할 수 있습니다.
//   }
//   [Object: null prototype] {
//     var1: 'newData',
//     var2: '153',
//     var3: 'testdata2017'
//   }
//  --- log end ---
//--- log start ---
// Url {
//     protocol: null,
//     slashes: null,
//     auth: null,
//     host: null,
//     port: null,
//     hostname: null,
//     hash: null,
//     search: null,
//     query: null,
//     pathname: '/favicon.ico',
//     path: '/favicon.ico',
//     href: '/favicon.ico'
//   }
//   [Object: null prototype] {}
//  --- log end ---
// 로그를 보면 --- log start --- 와 --- log end --- 가 두번 출력되었습니다. 이유는 서버로 요청시 기본적으로 표시되는 favicon 이라는 아이콘을 브라우저에서 요청하였기 때문입니다. 네이버 홈페이지를 예로 들면 페이지 타이틀 앞에 녹색 아이콘이 favicon 이다.
  


//소스코드 분석
//node.js에는 console과 같은 내장객체와 더불어 미리 정의되어 있는 내장 module 이 있다. 그중 url은 클라이언트가 요청한 주소 값을 javascript 객체로 변환해서 사용할 수 있게 하는 모듈

// var url = require('url');
// querystring은 주소로 전달된 Query String 을 변환해서 javascript 객체로 사용할 수 있게 해주는 모듈

// var querystring = require('querystring');
// 이 코드에서는 서버에서 처리되는 내용을 콘솔화면에 출력하는데 로그의 시작과 끝을 알기 위해서 아래와 같이 로그의 시작과 끝을 출력

//console.log('--- log start ---');

//...

//console.log('--- log end ---');
// url 모듈의 parse()함수를 사용해서 request 객체에 있는 url 값을 parsedUrl 변수에 담은후에 로그로 출력합니다.
// 위의 로그분석의 내용과 같이 parsedUrl 객체에 담겨 있는 내용이 출력
// request 객체 내부에 url 이라는 변수가 존재하고 이 url 변수는 주소를 문자열 값으로 가지고 있음

// var parsedUrl = url.parse(request.url);
// console.log(parsedUrl);
//(참고) https://javafa.gitbooks.io/nodejs_server_basic/content/appendix2_http_request_object.html

// parsedUrl 에는 객체화된 url 값이 들어가 있습니다. 이중에 Query String 이 담겨있는 query 변수를 가져온 후 querystring 모듈의 parse() 함수를 이용해서 객체화합니다. 역시 log() 함수를 이용해서 객체를 출력하면 Query String 으로 전달된 변수와 값이 로그분석과 같이 출력이 됨

// var parsedQuery = querystring.parse(parsedUrl.query,'&','=');
// console.log(parsedQuery);

//querystring.parse() 함수를 보면 첫번째 값으로는 parsedUrl 의 query, 두번째 값으로는 문자열 '&', 세번째 값으로는 문자열 '='이 입력되었습니다. 이렇게 parsing될 Query String과 함께 구분자를 지정해 줄 수 있는데, 이렇게 입력되는 구분자에 따라 '&'와 '='이 아니더라도 parsing을 해줄수있다.

// var result1 = parsedQuery.var1;
// console.log('전달된 var1의 값은 ' +result1+'입니다');
// parsedQuery 에 담긴 객체를 아래와 같이 paresdQuery.var1 형태로 사용할 수 있다.

// 브라우저에 요청값 돌려주기

// response.writeHead(200, {'Content-Type':'text/html'});
// response.end('var1의 값은 '+parsedQuery.var1);
// 코드를 수정해줍니다.