var module = require('./custom_module');

// 1. formatted 특수문자 %d를 사용해서 module.sum()에서 리턴된 숫자값을 출력
console.log('sum = %d', module.sum(100));

// 2. formatted 특수문자 %s를 사용해서 module.var1의 문자값을 출력
console.log('var1 = %s',module.var1);

// 코드분석
// node.js에서 모듈형태로 사용하기 위해서는 내장객체인 exports를 사용하면 됩니다. exports 객체로 먼저 사용할 변수명을 선언하고 해당 변수명에 함수,값 또는 객체를 대입해서 사용할 수 있습니다.

// 첫번째 예저 custom_module.js 는 함수와 값을 대입해보았는데 함수형 언어에서 함수는 객체지향의 객체와 같이 각각의 함수가 객체처럼 포인터를 가진다. 따라서 예제처럼 변수에 대입하는 것이 가능하다.

// 함수를 객체처럼 사용하기
// var 변수명 = function(파라미터){함수식}

// 두번째 예제 home.js에서는 위의 모듈을 직접 호출해서 사용하게 되는데 node.js의 console.log()함수는 C언어에서의 printf()처럼 formatted 출력을 지원한다. 따라서 출력할 문자열 중간에 %d,%s 등을 사용해서 두번째부터 입력되는 값을 문자열 내부에 출력할 수 있습니다. C에서와 같이 여러개의 입력문자 또는 숫자를 한번에 처리할 수 있다.

// 문자열 출력 여러개 하기
//console.log('var1 = %d, var2 = %s, var3 = %d', 105, 'Hello!', 10098);
