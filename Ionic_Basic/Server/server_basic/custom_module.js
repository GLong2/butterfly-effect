//module 사용
//require('모듈이름')
// js파일을 모듈화하기 위해 사용하는 내장객체입니다. node.js에서 모듈은 파일구조입니다. 앞장에서 사용했던 http 모듈을 예로 살펴보면 http 모듈은 특정폴더에 http.js 파일 또는 http 폴더에 index.js 파일로 존재하게 된다. 구조는 아래와 같다.

//require('모듈이름');

// / 모듈이름.js
// / 모듈이름 / index.js

// 위와 같이 require('모듈이름')으로 모듈을 호출하면 먼저 '모듈이름.js' 파일이 경로상에 있는지 검색하고 없으면 '모듈이름'으로된 폴더를 검색한다. 그리고 해당 폴더 아래에 index.js 파일을 모듈로 가져와서 사용하게 된다.

// exports로 모듈 만들기

// 1. exports 객체를 사용해서 sum이라는 변수를 만들고, sum 변수에 function 을 사용해서 하나의 파라미터를 가진 함수식을 대입
exports.sum = function(max){
    // 2. 입력된 값을 최대값으로 1부터 최대값까지 더해서 반환하는 로직
    return (max+1)*max/2;
}

// 3. var1 변수에 'NEW VALUE100' 입력
exports.var1 = 'NEW VALUE100';