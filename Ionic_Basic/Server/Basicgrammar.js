// 변수의 선언 및 사용
// 변수는 타입에 대한 구분없이 앞에 var 로 선언하고 사용한다. 변수의 타입은 실행시에 스크립트 해석기인 엔진이 결정하지만 V8엔진의 경우 실행시가 아닌 JIT compiler가 기계어로 컴파일하기 직전에 컴파일러에 의해서 결정

//1. 변수는 타입에 상관없이 앞에 var로 선언한다.
var name;
var num;

//2. = 를 사용해서 변수에 값을 입력하는데 문자열 입력시에는 앞뒤로 ' 또는 " 를 붙여준다
name = '홍길동';
num = 12;

//3. 변수의 선언과 동시에 값을 입력할 수 있다.
var num2 = 3;

//4. 두 개의 변수를 더해서 다른 변수에 입력할 수 있다.
var sum = num + num2;

//5. 숫자와 문자를 더할 경우 결과값은 문자가 된다. 아래 연산결과로 sum2 에는 "홍길동12"이 sum3에는 "이순신3"이 입력된다.
var sum2 = name + num;
var sum3 = '이순신'+3;

// 함수의 선언 및 사용
//함수는 function 함수이름(파라미터){실행코드} 형태로 선언하며, 함수이름을 괄호와 함께 호출하면 실행 

// 1. 세개의 파라미터를 더한 후 결과값을 리턴하는 함수를 선언
function sum(param1,param2,param3){
    return param1+param2+param3;
}
console.log(sum);//sum의 값 출력
//2. 함수 실행 후 결과값을 result에 대입
var result = sum(1,2,3);

//3. result 에 담긴 결과값을 출력
console.log('result='+result);

//4. 결과값이 없는 함수의 선언
function print(param1){
    console.log('parame='+param1);
}
//5. 함수호출 : return 이 없는 함수는 로직을 자체적으로 처리하기 때문에 결과값 대입 불필요
print('출력내용');

//조건문
//if와 switch가 있다.
var a = 10;

if (a > 11){
    console.log('a가 11보다 크다');
}else if(a === 11){
    console.log('a가 11과 같다');
}else{
    console.log('a가 11보다 작다');
}

//반복문
// 반복문은 for와 while, do ~ while이 있음
var i = 0 ;
while(i<10){
    console.log("for : i의 값은="+i);
    i=i+1;
}

//클래스 
// 자바스크립트는 프로토타입기반의 함수형 언어이기 때문에 특별히 객체지향을 위한 class는 없다
// 하지만 함수형 언어들의 특징으로 함수자체를 하나의 객체로 취급하기 때문에 단일함수 또는 파일 자체를 하나의 class 처럼 사용이 가능하다.
