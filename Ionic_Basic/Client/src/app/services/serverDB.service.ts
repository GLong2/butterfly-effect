import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BoardDB } from './boardDB';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class ServerDBService {
    private storage: SQLiteObject;
    contentsList = new BehaviorSubject([]);
    private isDBReady: BehaviorSubject<boolean> = new BehaviorSubject(false);


    constructor(
        private platform: Platform,
        private sqlite: SQLite,
        private httpClient: HttpClient,
        private sqlPorter: SQLitePorter,
    ) {
        this.platform.ready().then(() => {
            this.createDB();
        }).catch(error => {
            console.log(error);
        })


    }

    dbState() {
        return this.isDBReady.asObservable();
    }

    fetchContents(): Observable<BoardDB[]> {
        return this.contentsList.asObservable();
    }

    createDB() {
        this.sqlite.create({
            name: 'board_db.db',
            location: 'default'
        })
            .then((db: SQLiteObject) => {
                this.storage = db;
                this.getDataFromDB();
                console.log('DB Create Done');
            })
            .catch(e => {
                alert("error " + JSON.stringify(e))
            });

            console.log(this.storage);
    }

    // 초기 DB생성
    getDataFromDB() {
        let option = {};
        this.httpClient.get(
            'http://125.135.56.9:xxxx/..........', option
        ).subscribe((data: any) => {
            var items = data;
            if(items.length>0){
                items.array.forEach(element => {
                    
                });
            }
        });
    }

    // Get list
    getContents() {
        return this.storage.executeSql('SELECT * FROM boardtable', []).then(res => {
            let items: BoardDB[] = [];              //조회결과를 넣을 리스트
            if (res.rows.length > 0) {              //조건 -> 결과조회값이 있을경우
                for (var i = 0; i < res.rows.length; i++) {
                    items.push({
                        id: res.rows.item(i).id,
                        title: res.rows.item(i).title,
                        img: res.rows.item(i).img,
                        content: res.rows.item(i).content
                    });
                }
            }
            this.contentsList.next(items);
        });
    }

    // Add
    addContents(title, img, content) {
        let data = [title, img, content];
        return this.storage.executeSql('INSERT INTO boardtable (title, img, content) VALUES (?, ?, ?)', data)
            .then(res => {
                this.getContents();  //생성 후 DB재조회
            });
    }

    // getValue
    getContent(id): Promise<BoardDB> {
        return this.storage.executeSql('SELECT * FROM boardtable WHERE id = ?', [id]).then(res => {
            return {
                id: res.rows.item(0).id,
                title: res.rows.item(0).title,
                img: res.rows.item(0).img,
                content: res.rows.item(0).content
            } //id로 DB에서 값조회후 리턴
        });
    }

    // Update
    updateContents(id, content: BoardDB) {
        let data = [content.title, content.img, content.content];
        return this.storage.executeSql(`UPDATE boardtable SET title = ?, img = ?, content = ? WHERE id = ${id}`, data)
            .then(data => {
                this.getContents(); //업데이트 후 DB재조회
            });
    }

    // Delete
    deleteContents(id) {
        return this.storage.executeSql('DELETE FROM boardtable WHERE id = ?', [id])
            .then(_ => {
                this.getContents(); //삭제 후 DB재조회
            });
    }
}
