import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BoardDB } from './boardDB';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class DbService {
    contentsList = new BehaviorSubject([]);
    private isDBReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

    boardContentsList: any = [];
    boardContent: any;
    tempdata: any;

    constructor(
        private router: Router,
        private platform: Platform,
        private httpClient: HttpClient,
    ) { }

    dbState() {
        return this.isDBReady.asObservable();
    }

    fetchContents(): Observable<BoardDB[]> {
        return this.contentsList.asObservable();
    }

    getContents() {
        this.boardContentsList = [];
        this.httpClient.get('http://125.135.56.9:52273/getcontents').subscribe((res: any) => {
            if (res.length > 0) {
                res.forEach((data) => {
                    this.boardContentsList.push({
                        id: data.id,
                        title: data.title,
                        contents: data.contents,
                        img: data.img
                    });
                });
            }
            //console.log(this.boardContentsList);
        });
        return this.boardContentsList;
    }

    // Add
    addContents(addTitle, addImg, addContents) {
        let option = {
            title: addTitle,
            img: addImg,
            contents: addContents
        }
        this.httpClient.post('http://125.135.56.9:52273/insert', option).subscribe((res: any) => {
            this.router.navigate(['board']);
        })
    }

    getContent(id) {
        this.boardContent = '';
        this.httpClient.get('http://125.135.56.9:52273/getcontent').subscribe((res: any) => {
            res.forEach((data) => {
                this.boardContentsList.push({
                    id: data.id,
                    title: data.title,
                    contents: data.contents,
                    img: data.img
                });
            });
            //console.log(this.boardContentsList);
        });
        return this.boardContentsList;
    }

    // Update
    updateContents(id, title, contents, img) {
        console.log("data 값");
        console.log(id);
        console.log(title);
        console.log(contents);
        console.log(img);

        let datas = {
            id: id,
            title: title,
            contents: contents,
            img: img
        }

        this.httpClient.post('http://125.135.56.9:52273/edit', datas).subscribe((res: any) => {

            this.router.navigate(['board']);
        });
    }


    // Delete
    deleteContents(data) {

        console.log("data 값");
        console.log(data);

        let datas = {
            id: data
        }

        this.httpClient.post('http://125.135.56.9:52273/delete', datas).subscribe((res: any) => {
        });
    }
}
