export class BoardDB {
    id: number;
    title: string;
    img: string;
    content: string;
}
