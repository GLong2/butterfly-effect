import { TestBed } from '@angular/core/testing';

import { ServerDBService } from './serverDB.service';

describe('ServerDBService', () => {
  let service: ServerDBService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServerDBService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
