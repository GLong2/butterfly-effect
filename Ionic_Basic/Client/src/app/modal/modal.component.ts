import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss'],
})
export class ModalComponent {

    testInput = new FormControl('', Validators.required);

    constructor(private modalController: ModalController) {

    }

    closeModal() {
        this.modalController.dismiss();
    }

    onSubmit() {

    }

}
