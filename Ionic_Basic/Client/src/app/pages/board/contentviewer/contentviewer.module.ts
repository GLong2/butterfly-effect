import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContentviewerPageRoutingModule } from './contentviewer-routing.module';

import { ContentviewerPage } from './contentviewer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContentviewerPageRoutingModule
  ],
  declarations: [ContentviewerPage]
})
export class ContentviewerPageModule {}
