import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentviewerPage } from './contentviewer.page';

const routes: Routes = [
  {
    path: '',
    component: ContentviewerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContentviewerPageRoutingModule {}
