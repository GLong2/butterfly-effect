import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, IonContent, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { DbService } from 'src/app/services/db.service';
import { FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-contentviewer',
    templateUrl: './contentviewer.page.html',
    styleUrls: ['./contentviewer.page.scss'],
})
export class ContentviewerPage implements OnInit {

    getValue: any;
    image: any;
    index: any

    logScrollStart(event) {
        // console.log("logScrollStart : When Scroll Starts", event);
    }

    @ViewChild(IonContent, { static: false }) content: IonContent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private alertController: AlertController,
        private db: DbService,
        public formBuilder: FormBuilder,
        public toastController: ToastController
    ) {
        if (this.router.getCurrentNavigation().extras.state) {
            this.getValue = this.router.getCurrentNavigation().extras.state.item;
        }
    }

    ionViewWillEnter() {
        if (Object.keys(this.getValue).length == 0) {
            this.router.navigateByUrl('', { replaceUrl: true, }).then(() => { });
        }
    }

    ngOnInit() { }

    gotoEditer(getValue) {
        this.router.navigateByUrl('board/contentediter', { state: { item: getValue }, replaceUrl: false, }).then(() => { });
    }

    ScrollToTop() {
        this.content.scrollToTop();
    }

    async DeleteContent() { //삭제
        const alert = await this.alertController.create({
            header: 'Warning',
            // subHeader: '삭제',
            message: '삭제시 데이터를 되돌릴수없습니다.',
            buttons: [
                {
                    text: '확인',
                    handler: () => {
                        console.log('Delete Done! :' + this.getValue.id);
                        this.db.deleteContents(this.getValue.id);

                        this.toastController.create({
                            message: 'Delete Done!!!',
                            duration: 2000
                        }).then((toastData) => {
                            console.log(toastData);
                            toastData.present();
                        });

                        this.router.navigate(['board']);

                    }
                },
                {
                    text: '취소',
                    handler: () => {
                        console.log('Delete cancel');
                    }
                }
            ]
        });
        await alert.present();
    }

}
