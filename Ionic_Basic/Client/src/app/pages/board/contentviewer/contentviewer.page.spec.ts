import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContentviewerPage } from './contentviewer.page';

describe('ContentviewerPage', () => {
  let component: ContentviewerPage;
  let fixture: ComponentFixture<ContentviewerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentviewerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContentviewerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
