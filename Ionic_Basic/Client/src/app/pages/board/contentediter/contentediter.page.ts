import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, FormsModule, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { DbService } from 'src/app/services/db.service';

@Component({
    selector: 'app-contentediter',
    templateUrl: './contentediter.page.html',
    styleUrls: ['./contentediter.page.scss'],
})
export class ContentediterPage implements OnInit {

    mainForm: FormGroup;
    getValue: any;
    viewerValue: any;
    isSubmitted = false;
    index: any;
    image: any;

    constructor(
        public formBuilder: FormBuilder,
        private db: DbService,
        private router: Router,
        private route: ActivatedRoute,
        public toastController: ToastController
    ) {
        if (this.router.getCurrentNavigation().extras.state) {
            this.viewerValue = this.router.getCurrentNavigation().extras.state.item;
        }
    }

    ngOnInit() {

        if (this.route.snapshot.paramMap.get('id') != '') {

            this.getValue = this.formBuilder.group({
                id: [],
                title: [''],
                contents: [''],
                img: ['']
            })

            // this.index = this.route.snapshot.paramMap.get('id');


            this.getValue.setValue({
                id: this.viewerValue.id,
                title: this.viewerValue.title,
                contents: this.viewerValue.contents,
                img: this.viewerValue.img
            })
        }

        this.mainForm = new FormGroup({
            title: new FormControl('', [Validators.required, Validators.minLength(1)]),
            contents: new FormControl('', [Validators.required, Validators.minLength(1)]),
            img: new FormControl('')
        })
    }

    get errorContorl() {
        return this.mainForm.controls;
    }

    SubmitData() {
        this.isSubmitted = true;
        if (!this.mainForm.valid) {
            console.log('Please check required values')

            this.toastController.create({
                message: 'Please check required values',
                duration: 2000
            }).then((toastData) => {
                console.log(toastData);
                toastData.present();
            });

            return false;
        } else {
            if (this.mainForm.value.img == '') {
                this.mainForm.value.img = "https://i.stack.imgur.com/y9DpT.jpg";
            }
            this.db.updateContents(
                this.getValue.value['id'],
                this.getValue.value['title'],
                this.getValue.value['contents'],
                this.getValue.value['img']
            )

            this.toastController.create({
                message: '수정완료',
                duration: 2000
            }).then((toastData) => {
                console.log(toastData);
                toastData.present();
            });

            this.router.navigate(['board/', this.index])

        }
    }

}
