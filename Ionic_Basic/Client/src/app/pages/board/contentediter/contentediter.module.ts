import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContentediterPageRoutingModule } from './contentediter-routing.module';

import { ContentediterPage } from './contentediter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContentediterPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ContentediterPage],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ContentediterPageModule {}
