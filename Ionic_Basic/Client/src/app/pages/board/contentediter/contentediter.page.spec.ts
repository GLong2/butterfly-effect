import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContentediterPage } from './contentediter.page';

describe('ContentediterPage', () => {
  let component: ContentediterPage;
  let fixture: ComponentFixture<ContentediterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentediterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContentediterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
