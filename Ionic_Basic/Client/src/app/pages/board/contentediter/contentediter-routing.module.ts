import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentediterPage } from './contentediter.page';

const routes: Routes = [
  {
    path: '',
    component: ContentediterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContentediterPageRoutingModule {}
