import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContentcreatorPageRoutingModule } from './contentcreator-routing.module';

import { ContentcreatorPage } from './contentcreator.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContentcreatorPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ContentcreatorPage],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ContentcreatorPageModule {}
