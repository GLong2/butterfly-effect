import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, FormsModule, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { DbService } from 'src/app/services/db.service';

@Component({
    selector: 'app-contentcreator',
    templateUrl: './contentcreator.page.html',
    styleUrls: ['./contentcreator.page.scss'],
})
export class ContentcreatorPage implements OnInit {

    mainForm: FormGroup;
    getValue: any;
    isSubmitted = false;
    index: any;
    image: any;

    constructor(
        public formBuilder: FormBuilder,
        private db: DbService,
        private router: Router,
        private route: ActivatedRoute,
        public toastController: ToastController
    ) { }

    ngOnInit() {

        // this.route.queryParams.subscribe(params => {
        //     console.log('params: ', params);
        //     if (params && params.special) {
        //         this.getValue = JSON.parse(params.special);
        //     }
        // });

        this.mainForm = new FormGroup({
            title: new FormControl('', [Validators.required, Validators.minLength(1)]),
            content: new FormControl('', [Validators.required, Validators.minLength(1)]),
            img: new FormControl('')
        })
    }

    get errorContorl() {
        return this.mainForm.controls;
    }

    SubmitData() {
        this.isSubmitted = true;
        if (!this.mainForm.valid) {
            console.log('Please check required values')

            this.toastController.create({
                message: 'Please check required values',
                duration: 2000
            }).then((toastData) => {
                console.log(toastData);
                toastData.present();
            });

            return false;
        } else {
            if (this.mainForm.value.img == '') {
                this.mainForm.value.img = "https://i.stack.imgur.com/y9DpT.jpg";
            }
            this.db.addContents(
                this.mainForm.value.title,
                this.mainForm.value.img,
                this.mainForm.value.content
            );
        }
    }

    // SubmitData() {
    //     this.isSubmitted = true;
    //     if (!this.mainForm.valid) {
    //         console.log('Please check required values')

    //         this.toastController.create({
    //             message: 'Please check required values',
    //             duration: 2000
    //         }).then((toastData) => {
    //             console.log(toastData);
    //             toastData.present();
    //         });

    //         return false;
    //     } else {
    //         if (this.mainForm.value.img == '') {
    //             this.mainForm.value.img = "https://i.stack.imgur.com/y9DpT.jpg";
    //         }
    //         this.db.addContents(
    //             this.mainForm.value.title,
    //             this.mainForm.value.img,
    //             this.mainForm.value.content
    //         ).then((res) => {
    //             this.mainForm.reset();

    //             this.toastController.create({
    //                 message: '작성완료',
    //                 duration: 2000
    //             }).then((toastData) => {
    //                 console.log(toastData);
    //                 toastData.present();
    //             });

    //             this.router.navigate(['board'])
    //         })
    //     }
    // }

}
