import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContentcreatorPage } from './contentcreator.page';

describe('ContentcreatorPage', () => {
  let component: ContentcreatorPage;
  let fixture: ComponentFixture<ContentcreatorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentcreatorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContentcreatorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
