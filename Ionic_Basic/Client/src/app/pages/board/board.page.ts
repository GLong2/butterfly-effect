import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { Router } from '@angular/router';
import { DbService } from './../../services/db.service';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-board',
    templateUrl: './board.page.html',
    styleUrls: ['./board.page.scss'],
})
export class BoardPage implements OnInit {

    logScrollStart(event) { //스크롤이벤트
        // console.log("logScrollStart : When Scroll Starts", event);
    }

    @ViewChild(IonContent, { static: false }) content: IonContent;

    boarddummy: any;
    boardcontent: any = [];

    constructor(private router: Router,
        private db: DbService,
        private httpClient: HttpClient
    ) { }

    ngOnInit() { }

    ionViewWillEnter() {
        console.log("test json load");
        this.boardcontent = [];
        this.boardcontent = this.db.getContents();
        console.log(this.boardcontent);
    }

    gotoViewer(content) {
        console.log(content);
        this.router.navigateByUrl('board/contentviewer', { state: { item: content }, replaceUrl: false, }).then(() => { });
    }

    ScrollToTop() {
        this.content.scrollToTop();
    }

    CreateContent() {
        //작성버튼
        this.router.navigate(['contentcreator'])
    }



}
