import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';   //ngModel 사용 필수 import

import { IonicModule } from '@ionic/angular';

import { TestpagePageRoutingModule } from './testpage-routing.module';

import { TestpagePage } from './testpage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,        //ngModel 사용 필수 import 
    ReactiveFormsModule,
    IonicModule,
    TestpagePageRoutingModule
  ],
  declarations: [TestpagePage]
})
export class TestpagePageModule {}
