import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { TestpagePage } from './testpage.page';

const routes: Routes = [
  {
    path: '',
    component: TestpagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), ReactiveFormsModule],
  exports: [RouterModule],
})
export class TestpagePageRoutingModule {}
