import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from '../../modal/modal.component';


@Component({
    selector: 'app-testpage',
    templateUrl: './testpage.page.html',
    styleUrls: ['./testpage.page.scss'],
})
export class TestpagePage implements OnInit {

    InputTest: any;

    //test value///////////////
    tests = [
        {
            name: 'UseBy',
            age: 28
        },
        {
            name: 'BananaBoy',
            age: 29
        },
        {
            name: 'GeomScene',
            age: 30
        }
    ];
    /////////////////////////////

    constructor(private modalController: ModalController) { }

    ngOnInit() {

    }

    async openModal() { //async : javascript 비동기 처리패턴
        const modal = await this.modalController.create({
            component: ModalComponent
        });
        await modal.present();
    }

}
