export interface Boardcontent {
    index: number;
    title: string;
    image: string;
    content: string;
}
