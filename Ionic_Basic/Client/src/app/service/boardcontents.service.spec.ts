import { TestBed } from '@angular/core/testing';

import { BoardcontentsService } from './boardcontents.service';

describe('BoardcontentService', () => {
  let service: BoardcontentsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BoardcontentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
