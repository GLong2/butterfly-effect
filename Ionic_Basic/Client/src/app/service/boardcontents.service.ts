import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Boardcontent } from '../model/boardcontent';

@Injectable({
    providedIn: 'root'
})
export class BoardcontentsService {

    constructor(private http: HttpClient) { }

    getContents(): Observable<Boardcontent[]> {
        return this.http.get<Boardcontent[]>('./assets/data/boardcontent.json');
    }
}