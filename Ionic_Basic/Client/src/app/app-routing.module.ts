import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'inbox',
        loadChildren: () => import('./pages/inbox/inbox.module').then(m => m.InboxPageModule)
    },
    {
        path: 'outbox',
        loadChildren: () => import('./pages/outbox/outbox.module').then(m => m.OutboxPageModule)
    },
    {
        path: 'account',
        loadChildren: () => import('./tabs/account/account.module').then(m => m.AccountPageModule)
    },
    {
        path: 'calls',
        loadChildren: () => import('./tabs/calls/calls.module').then(m => m.CallsPageModule)
    },
    {
        path: 'messages',
        loadChildren: () => import('./tabs/messages/messages.module').then(m => m.MessagesPageModule)
    },
    {
        path: 'testpage',
        loadChildren: () => import('./pages/testpage/testpage.module').then(m => m.TestpagePageModule)
    },
    {
        path: 'board/contentviewer',
        loadChildren: () => import('./pages/board/contentviewer/contentviewer.module').then(m => m.ContentviewerPageModule)
    },
    {
        path: 'board/contentediter',
        loadChildren: () => import('./pages/board/contentediter/contentediter.module').then(m => m.ContentediterPageModule)
    },
    {
        path: 'board',
        loadChildren: () => import('./pages/board/board.module').then(m => m.BoardPageModule)
    },
    {
        path: 'contentcreator',
        loadChildren: () => import('./pages/board/contentcreator/contentcreator.module').then(m => m.ContentcreatorPageModule)
    },

];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
